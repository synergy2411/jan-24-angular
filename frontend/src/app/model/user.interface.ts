import { IComment } from './coment.interface';

export interface IUser {
  firstName: string;
  lastName: string;
  dob: Date;
  income: number;
  image: string;
  isWorking: boolean;
  company: string;
  votes: number;
  comments: IComment[];
}
