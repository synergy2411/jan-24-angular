import { Component, DoCheck, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IExpenses } from 'src/app/model/expense.interface';
import { ExpenseService } from 'src/app/services/expense.service';

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.css'],
})
export class ExpenseComponent implements OnInit {
  expenses: IExpenses[] = [];

  constructor(
    private expService: ExpenseService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      if (params['update']) {
        this.getExpenses();
      }
    });
    this.getExpenses();
  }

  private getExpenses() {
    this.expService
      .getExpenses()
      .subscribe((expenses) => (this.expenses = expenses));
  }

  onEdit(expenseId: string | undefined) {
    this.router.navigate(['/expenses/' + expenseId]);
  }
}
