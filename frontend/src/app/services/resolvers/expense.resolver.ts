import { Injectable } from '@angular/core';
import {
  Router,
  Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
} from '@angular/router';
import { Observable, of } from 'rxjs';
import { IExpenses } from 'src/app/model/expense.interface';
import { ExpenseService } from '../expense.service';

@Injectable({
  providedIn: 'root',
})
export class ExpenseResolver implements Resolve<any> {
  constructor(private expService: ExpenseService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<IExpenses> {
    return this.expService.getExpense(route.params['expenseId']);
  }
}
