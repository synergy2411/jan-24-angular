import { Component } from '@angular/core';
import {
  FormControl,
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors,
  FormArray,
} from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css'],
})
export class AuthComponent {
  // username = new FormControl('', [Validators.required, Validators.email]);
  // password = new FormControl('', [
  //   Validators.required,
  //   Validators.minLength(6),
  // ]);

  authForm!: FormGroup;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    this.authForm = this.fb.group({
      username: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        AuthComponent.exclamationMark,
      ]),
      cnfPassword: new FormControl('', AuthComponent.confirmPassword),
      languages: this.fb.array([]),
    });
  }

  get username() {
    return this.authForm.get('username') as FormControl;
  }

  get password() {
    return this.authForm.get('password') as FormControl;
  }

  get cnfPassword() {
    return this.authForm.get('cnfPassword') as FormControl;
  }

  get languages() {
    return this.authForm.get('languages') as FormArray;
  }

  addLanguage() {
    this.languages.push(
      this.fb.group({
        name: '',
        experience: '',
      })
    );
    // console.log(this.authForm.get('languages'));
  }

  removeItem(ind: number) {
    this.languages.removeAt(ind);
  }

  onLogin() {
    console.log(this.authForm);
    debugger;
    this.authService
      .onLogin(this.username.value, this.password.value)
      .subscribe((data) => {
        console.log(data.message);
        this.router.navigate(['expenses']);
      });
  }

  static confirmPassword(control: AbstractControl): ValidationErrors | null {
    let isMatch: boolean = false;
    if (control.parent && control.parent.controls) {
      if (control.value === control.parent.get('password')?.value) {
        if (control.parent.get('password')?.value !== '') isMatch = true;
      }
    }
    return isMatch ? null : { confirmPassword: true };
  }

  static exclamationMark(control: AbstractControl): ValidationErrors | null {
    const hasExclamationMark = control.value.indexOf('!') >= 0;

    return hasExclamationMark ? null : { exclamationMark: true };
  }

  static specialSymbol(symbol: string) {
    return (control: AbstractControl): ValidationErrors | null => {
      let isError: boolean;

      isError = control.value.includes(symbol);

      return isError ? null : { specialSymbol: true };
    };
  }

  // static myMinLength(length: number) {
  //   return (control: AbstractControl): ValidationErrors | null => {
  //     const isError = control.value.length < length;
  //     return isError ? {myMinLength : true} : null;
  //   }
  // }
}
