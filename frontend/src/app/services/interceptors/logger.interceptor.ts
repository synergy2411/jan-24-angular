import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpHeaders,
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class LoggerInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    // console.log('[AUTH INTERCEPTOR WORKS]', request);

    const methods = ['POST', 'PUT', 'PATCH'];

    if (methods.includes(request.method)) {
      let clonedRequest = request.clone({
        headers: new HttpHeaders().set('Content-Type', 'application/json'),
      });
      return next.handle(clonedRequest);
    }
    return next.handle(request);
  }
}
