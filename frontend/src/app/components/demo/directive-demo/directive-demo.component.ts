import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directive-demo',
  templateUrl: './directive-demo.component.html',
  styleUrls: ['./directive-demo.component.css'],
})
export class DirectiveDemoComponent implements OnInit {
  dynamicArrayClasses = ['my-border', 'feature'];

  dynamicObjectClasses = {
    'my-border': true,
    feature: false,
  };

  dynamicStyle = {
    textTransform: 'uppercase',
  };

  username = 'John Doe';
  isAdmin = true;

  tab = 0;

  todoCollection = [
    { label: 'buy some jeans', status: true },
    { label: 'shop for grocery', status: false },
    { label: 'renew car insurance', status: true },
    { label: 'pot more plants', status: false },
  ];

  constructor() {}

  ngOnInit(): void {}

  onClassRemove() {
    this.dynamicArrayClasses.splice(0, 1);
  }
  onToggleClasses() {
    this.dynamicObjectClasses['my-border'] =
      !this.dynamicObjectClasses['my-border'];
    this.dynamicObjectClasses['feature'] =
      !this.dynamicObjectClasses['feature'];
  }

  onChangStyle() {
    this.dynamicStyle.textTransform = 'lowercase';
  }
}
