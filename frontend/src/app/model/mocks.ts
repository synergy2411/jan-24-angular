import { IUser } from './user.interface';

export const USER_DATA: Array<IUser> = [
  {
    firstName: 'bill',
    lastName: 'gates',
    dob: new Date('Dec 21, 1965'),
    income: 50000,
    isWorking: true,
    company: 'microsoft',
    image: './assets/bill.jpg',
    votes: 120,
    comments: [
      { stars: 5, body: 'Great work', author: 'john@test' },
      { stars: 4, body: 'Like your work 👍', author: 'jenny@test' },
    ],
  },
  {
    firstName: 'steve',
    lastName: 'jobs',
    dob: new Date('Jan 1, 1965'),
    income: 0,
    isWorking: false,
    company: 'Apple',
    image: './assets/steve.jpg',
    votes: 180,
    comments: [{ stars: 4, body: 'Like it', author: 'john@test' }],
  },
  {
    firstName: 'tim b.',
    lastName: 'lee',
    dob: new Date('Aug 12, 1965'),
    income: 30000,
    isWorking: true,
    company: 'world wide web',
    image: './assets/tim.jpg',
    votes: 80,
    comments: [],
  },
];
