import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ITodo } from '../model/todo.interface';
import { Observable, catchError, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TodoService {
  private baseUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getTodos(): Observable<ITodo[]> {
    return this.http.get<ITodo[]>(`${this.baseUrl}/todos`);
    // .pipe(
    // catchError((err) => {
    //   console.log('SERVICE ERROR -  ', err);
    //   // return of(err)
    //   return throwError(err);
    // })
    // );
  }

  createTodo(label: string) {
    return this.http.post<ITodo>(
      `${this.baseUrl}/todos`,
      JSON.stringify({ label })
    );
  }

  deleteTodo(id: string) {
    return this.http.delete(`${this.baseUrl}/todos/${id}`);
  }
}
