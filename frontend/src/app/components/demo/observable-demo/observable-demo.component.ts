import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import {
  AsyncSubject,
  Observable,
  Subscription,
  debounceTime,
  forkJoin,
  fromEvent,
  interval,
  map,
  mergeMap,
  take,
} from 'rxjs';

import { ajax } from 'rxjs/ajax';

interface IRepo {
  id: string;
  name: string;
}

@Component({
  selector: 'app-observable-demo',
  templateUrl: './observable-demo.component.html',
  styleUrls: ['./observable-demo.component.css'],
})
export class ObservableDemoComponent implements OnInit, AfterViewInit {
  @ViewChild('term') term!: ElementRef<HTMLInputElement>;

  repos: Array<IRepo> = [];

  constructor() {}

  unSub$!: Subscription;

  obs$ = new Observable((observer) => {
    setTimeout(() => {
      observer.next('First Package');
    }, 1000);
    setTimeout(() => {
      observer.next('Second Package');
    }, 2000);
    setTimeout(() => {
      observer.next('Third Package');
    }, 4000);
    setTimeout(() => {
      observer.next('Fourth Package');
    }, 6000);
    setTimeout(() => observer.complete(), 7000);
  });

  onSubscribe() {
    console.log('START');
    this.unSub$ = this.obs$.subscribe({
      next: (data) => console.log(data),
      complete: () => console.log('[COMPLETED]'),
    });
    console.log('END');
  }

  onUnsubscribe() {
    this.unSub$.unsubscribe();
  }

  ngAfterViewInit(): void {
    // let fromEvent$ = fromEvent(document, 'click');
    // fromEvent$.subscribe(console.log);

    const input$ = fromEvent(this.term.nativeElement, 'input');

    input$
      .pipe(
        debounceTime(2000),
        map((val: any) => val.target.value),
        mergeMap<any, Observable<Array<IRepo>>>((searchTerm) =>
          ajax.getJSON(`https://api.github.com/users/${searchTerm}/repos`)
        )
        // mergeAll()
      )
      .subscribe((repos) => {
        console.log(repos);
        this.repos = repos;
      });

    // ForkJoin
    const interval1$ = interval(1000).pipe(take(4));
    const interval2$ = interval(1500).pipe(take(3));

    forkJoin({ intervalOne: interval1$, intervalTwo: interval2$ }).subscribe(
      console.log
    );
  }

  ngOnInit(): void {
    // SUBJECTS
    // let subject = new Subject();

    // let subject = new BehaviorSubject(0);

    // let subject = new ReplaySubject(2);

    let subject = new AsyncSubject();

    subject.next(99);
    subject.next(100);
    subject.next(101);

    subject.subscribe((data) => console.log('Subscribe 1 : ', data));

    subject.next(102);

    subject.subscribe((data) => console.log('Subscribe 2 : ', data));

    subject.next(103);

    subject.complete();

    // let from$ = from([101, 102, 104, 108]);
    // console.log('START');
    // from$.subscribe(console.log);
    // console.log('END');
    // let of$ = of('Hello', 'World', 'Goodbye');
    // of$.subscribe((data) => console.log(data));
    // of$.subscribe(console.log);
    // let interval$ = interval(1000);
    // interval$
    //   .pipe(
    //     // take(5)
    //     // takeUntil(),
    //     // takeWhile((val) => val < 5)
    //     filter((val) => val % 2 === 0),
    //     take(10),
    //     map((val) => val * 2)
    //   )
    //   .subscribe({
    //     next: (data) => {
    //       console.log('Data : ', data);
    //     },
    //     error: (err) => {},
    //     complete: () => {
    //       console.log('COMPLETE');
    //     },
    //   });
  }
}
