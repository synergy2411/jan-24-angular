import {
  Component,
  OnInit,
  Optional,
  Self,
  ViewEncapsulation,
} from '@angular/core';
import { IUser } from 'src/app/model/user.interface';
import { DemoService } from 'src/app/services/demo.service';
import { UserService } from 'src/app/services/user.service';
// import { USER_DATA } from 'src/app/model/mocks';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [DemoService],
  // viewProviders : []       // - specific to @Host()
})
export class UsersComponent implements OnInit {
  users!: Array<IUser>;

  constructor(
    private userService: UserService,
    @Self() private demo: DemoService
  ) {}

  ngOnInit(): void {
    // this.users = USER_DATA;
    this.users = this.userService.getUserData();
    console.log(this.demo.counter);
  }

  onMoreInfo(user: IUser) {
    alert(`Mr. ${user.lastName} is working with ${user.company}`);
  }
}
