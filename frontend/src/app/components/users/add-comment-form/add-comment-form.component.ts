import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { IComment } from 'src/app/model/coment.interface';

@Component({
  selector: 'app-add-comment-form',
  templateUrl: './add-comment-form.component.html',
  styleUrls: ['./add-comment-form.component.css'],
})
export class AddCommentFormComponent {
  @Output() newCommentEvent = new EventEmitter<IComment>();

  onSubmit(myForm: NgForm) {
    const newComment = <IComment>myForm.value;
    this.newCommentEvent.emit(newComment);
  }

  onReset(myForm: NgForm) {
    myForm.resetForm();
  }
}
