import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  Renderer2,
} from '@angular/core';

@Directive({
  selector: '[appHighlight]',
})
export class HighlightDirective {
  @Input() favColor: string = 'grey';

  @HostBinding('style.backgroundColor') bgColor!: string;

  @HostListener('mouseenter')
  onMouseEnter() {
    this.bgColor = this.favColor;
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.bgColor = 'transparent';
  }
  // constructor(private elRef: ElementRef, private renderer: Renderer2) {
  //   // this.elRef.nativeElement.style.backgroundColor = 'goldenrod';
  //   this.renderer.addClass(this.elRef.nativeElement, 'my-class');
  //   this.renderer.setStyle(this.elRef.nativeElement, 'backgroundColor', 'aqua');
  // }
}

// <div appHighlight></div>

// <div class='appHighlight'></div>
