import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
} from '@angular/common/http';
import { Observable, catchError, tap, throwError } from 'rxjs';

@Injectable()
export class ResponseInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(
    request: HttpRequest<unknown>,
    next: HttpHandler
  ): Observable<HttpEvent<unknown>> {
    return (
      next
        .handle(request)
        // .pipe(tap((response) => console.log('[RESPONSE INTERCEPTOR]', response)));
        .pipe(
          catchError((error: HttpErrorResponse) => {
            let errorMsg = '';

            if (error.error instanceof ErrorEvent) {
              console.log('Client Side ERROR');
              errorMsg = `Client ERROR : ${error.error.message}`;
            } else {
              console.log('Server side ERROR');
              errorMsg = `ERROR Status : ${error.status}, Message : ${error.message}`;
            }
            return throwError(() => errorMsg);
          })
        )
    );
  }
}

// ng g interceptor services/interceptors/logger
