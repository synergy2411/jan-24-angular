import { Routes } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';
import { ExpenseComponent } from './components/expense/expense.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TodosComponent } from './components/todos/todos.component';
import { UsersComponent } from './components/users/users.component';
import { EditExpenseComponent } from './components/expenses/edit-expense/edit-expense.component';
import { LoginGuard } from './services/guards/login.guard';
import { ExpenseResolver } from './services/resolvers/expense.resolver';

export const APP_ROUTES: Routes = [
  {
    path: '', // http://localhost:4200/
    redirectTo: 'auth',
    pathMatch: 'full',
  },
  {
    path: 'users', // http://localhost:4200/users
    component: UsersComponent,
  },
  {
    path: 'todos', // http://localhost:4200/todos
    component: TodosComponent,
  },
  {
    path: 'auth', // http://localhost:4200/auth
    component: AuthComponent,
  },
  {
    path: 'lazy',
    loadChildren: () =>
      import('./modules/lazy/lazy.module').then((m) => m.LazyModule),
  },
  {
    path: 'expenses', // http://localhost:4200/expenses
    canActivate: [LoginGuard],
    component: ExpenseComponent,
    children: [
      {
        path: ':expenseId', // http://localhost:4200/expenses/e001
        component: EditExpenseComponent,
        resolve: {
          expense: ExpenseResolver,
        },
      },
    ],
  },
  {
    path: '**', // http://localhost:4200/notexist
    component: PageNotFoundComponent,
  },
];
