import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-demo',
  templateUrl: './pipe-demo.component.html',
  styleUrls: ['./pipe-demo.component.css'],
})
export class PipeDemoComponent implements OnInit {
  greeting: string = 'Hello World!';

  amount: number = 90000;

  dob: Date = new Date('Dec 18, 1965');

  fraction: number = 123.4567;

  marks: number = 98;

  user = { email: 'test@test.com', dob: new Date('Dec 19, 1965') };

  promiseObj = new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve('Data arrived');
    }, 2000);
  });

  contactNumber = 98765431;

  filteredStatus = '';
  sortOrder = '';
  todoCollection = [
    { label: 'shopping', status: 'pending' },
    { label: 'insurance', status: 'completed' },
    { label: 'planting', status: 'pending' },
    { label: 'grocery', status: 'completed' },
  ];

  onAddNew() {
    this.todoCollection.push({ label: 'New Value', status: 'pending' });
  }
  constructor() {}

  ngOnInit(): void {}
}
