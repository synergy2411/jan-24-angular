import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IExpenses } from '../model/expense.interface';

@Injectable({
  providedIn: 'root',
})
export class ExpenseService {
  private baseUrl = 'http://localhost:3000';

  constructor(private http: HttpClient) {}

  getExpenses() {
    return this.http.get<IExpenses[]>(`${this.baseUrl}/expenses`);
  }

  getExpense(expenseId: string) {
    return this.http.get<IExpenses>(`${this.baseUrl}/expenses/${expenseId}`);
  }

  updateExpense(expense: IExpenses) {
    return this.http.patch(
      `${this.baseUrl}/expenses/${expense.id}`,
      JSON.stringify(expense)
    );
  }
}
