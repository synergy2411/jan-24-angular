import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AuthComponent } from './components/auth/auth.component';
import { DirectiveDemoComponent } from './components/demo/directive-demo/directive-demo.component';
import { LifeCycleComponent } from './components/demo/life-cycle/life-cycle.component';
import { ObservableDemoComponent } from './components/demo/observable-demo/observable-demo.component';
import { PipeDemoComponent } from './components/demo/pipe-demo/pipe-demo.component';
import { ViewEncapsulationComponent } from './components/demo/view-encapsulation/view-encapsulation.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { TodosComponent } from './components/todos/todos.component';
import { ButtonComponent } from './components/ui/button/button.component';
import { DialogComponent } from './components/ui/dialog/dialog.component';
import { AddCommentFormComponent } from './components/users/add-comment-form/add-comment-form.component';
import { CommentsComponent } from './components/users/comments/comments.component';
import { UserImgComponent } from './components/users/user-img/user-img.component';
import { UserInfoComponent } from './components/users/user-info/user-info.component';
import { UsersComponent } from './components/users/users.component';
import { HighlightDirective } from './directives/highlight.directive';
import { CountryCodePipe } from './pipes/country-code.pipe';
import { FilterPipe } from './pipes/filter.pipe';
import { ReversePipe } from './pipes/reverse.pipe';
import { SortPipe } from './pipes/sort.pipe';
import { LoggerInterceptor } from './services/interceptors/logger.interceptor';
import { ResponseInterceptor } from './services/interceptors/response.interceptor';
import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './app.routing';
import { NavigationComponent } from './components/navigation/navigation.component';
import { ExpenseComponent } from './components/expense/expense.component';
import { EditExpenseComponent } from './components/expenses/edit-expense/edit-expense.component';
import { EagerModule } from './modules/eager/eager.module';
@NgModule({
  declarations: [
    // Components , Directives , Pipes
    AppComponent,
    UsersComponent,
    UserImgComponent,
    UserInfoComponent,
    DialogComponent,
    ButtonComponent,
    LifeCycleComponent,
    ViewEncapsulationComponent,
    DirectiveDemoComponent,
    CommentsComponent,
    HighlightDirective,
    PipeDemoComponent,
    CountryCodePipe,
    ReversePipe,
    FilterPipe,
    SortPipe,
    AddCommentFormComponent,
    AuthComponent,
    ObservableDemoComponent,
    TodosComponent,
    PageNotFoundComponent,
    NavigationComponent,
    ExpenseComponent,
    EditExpenseComponent,
  ],
  imports: [
    // Module
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule.forRoot(APP_ROUTES),
    EagerModule,
  ],
  // providers: [UserService], // Services
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: LoggerInterceptor,
      multi: true,
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ResponseInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
