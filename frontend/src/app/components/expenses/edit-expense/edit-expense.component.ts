import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IExpenses } from 'src/app/model/expense.interface';
import { ExpenseService } from 'src/app/services/expense.service';

@Component({
  selector: 'app-edit-expense',
  templateUrl: './edit-expense.component.html',
  styleUrls: ['./edit-expense.component.css'],
})
export class EditExpenseComponent implements OnInit {
  expense!: IExpenses;

  constructor(
    private route: ActivatedRoute,
    private expService: ExpenseService,
    private router: Router
  ) {}

  ngOnInit(): void {
    // this.route.params.subscribe((params) => {
    //   this.expService
    //     .getExpense(params['expenseId'])
    //     .subscribe((expense) => (this.expense = expense));
    // });

    this.expense = this.route.snapshot.data['expense'];
  }

  onCancel() {
    this.router.navigate(['/expenses']);
  }

  onUpdate() {
    this.expService.updateExpense(this.expense).subscribe((response) => {
      console.log(response);
      this.router.navigate(['/expenses'], { queryParams: { update: true } });
    });
  }
}
