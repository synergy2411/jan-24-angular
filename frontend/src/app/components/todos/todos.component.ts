import { Component, OnInit } from '@angular/core';
import { ITodo } from 'src/app/model/todo.interface';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css'],
})
export class TodosComponent implements OnInit {
  todoCollection!: ITodo[];

  enteredLabel: string = '';

  errMessage = '';

  pointerStyle = {
    cursor: 'pointer',
  };

  constructor(private todoService: TodoService) {}

  ngOnInit(): void {
    this.todoService.getTodos().subscribe({
      next: (todos) => (this.todoCollection = todos),
      error: (err) => (this.errMessage = err),
    });
  }

  onAddTodo() {
    if (this.enteredLabel.trim() === '') {
      return;
    }
    this.todoService.createTodo(this.enteredLabel).subscribe((createdTodo) => {
      this.todoCollection.push(createdTodo);
      this.enteredLabel = '';
    });
  }

  onDeleteTodo(todoId: string) {
    this.todoService.deleteTodo(todoId).subscribe((result) => {
      console.log('DELETE RESULT : ', result);
      const position = this.todoCollection.findIndex(
        (todo) => todo.id === todoId
      );
      this.todoCollection.splice(position, 1);
    });
  }
}
