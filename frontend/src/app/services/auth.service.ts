import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  isLoggedIn = false;

  constructor() {}

  onLogin(email: string, password: string) {
    return new Observable<{ message: string }>((observer) => {
      // Verify the user from Server here
      setTimeout(() => {
        if (email === 'john@test' && password === 'john!123') {
          this.isLoggedIn = true;
          observer.next({ message: 'Logged In' });
        } else {
          observer.error(new Error('Bad Credentials'));
        }
      }, 1500);
    });
  }

  isAuthenticated() {
    return this.isLoggedIn;
  }
}
