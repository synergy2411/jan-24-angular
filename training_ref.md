# Break Timing

- Tea : 11:00 (15 minutes)
- Lunch : 1:00 (45 minutes)
- Tea : 3:30 (15 minutes)

# What is JavaScript?

- Dynamic Language
- Loosely Typed
- Single Threaded
- DOM Manipulation
- Scripting Language
- Asynchronous / Non-blocking
- OOPS Concepts / ES2015 features / ES6 Features
- Promise / Async...await
- Event based
- Both Client as well as Server Side

# JavaScript Types

- Primitive : number, string, boolean, bigint, symbol
- Reference : object, array, function, date

# TypeScript

- JavaScript with the syntax of typings
- unknown, any, void, undefined, null, tuple, enum, never etc
- Custom Types : type keyword / interface / classes

# Other JS Libraries / Framework

# Client Side

- Angular (latest version) : Template, 2 way data binding, State Management, SPA, Form Validation, AJAX Calls, DOM Manipulation, Animation, AOT Compiler, MVC Pattern, Component based, Shadow DOM (Encapsulation), Web Components, Content Propjection (Slot API) etc

- AngularJS (v1.x) : library
- React : Hooks (v16.8), Redux - State Management, Props, Component based, Virtual DOM, learning path, very fast, reconciliation process, Diffing Algorithm
  > render the UI quickly and efficiently
  > formik, yup, react-hook-form etc for form validation
  > react-router-dom for SPA
  > axios / fetch API for AJAX Calls
- Vue : Template oriented, Framework, "Evan You"
- jQuery : DOM manipulation, AJAX, Animation
- NextJS : Framework to render React app on Server
- EmberJS : Framework, Frequent API changes
- Knockout : MVVM Pattern, 2 way data binding
- BackboneJS : MVC at Client Side
- Stencil : Vitrual DOM, Component based
- Polymer : Custom Web Components

# Server Side

- NodeJS : Platform (NRM, NPM, Node module)
- Express / Koa / Hapi / SailsJS / KrakenJS : Web framework runs on Node

# to install CLI Tool

> npm install @angular/cli@14 -g
> npm install @angular/cli@latest -g (v17.x)

> ng version

> ng new frontend

# for v17.x

> ng new frontend --no-standalone [This will generate the AppModule file]
> cd frontend
> npm start / ng serve

> ng generate help
> ng generate component users || ng g c components/users
> ng g d directiveName
> ng g p pipeName

# Bootstrap

- npm install bootstrap
- angular.json -> styles -> ./node_modules/bootstrap/dist/css/bootstrap.min.css
- restart the dev server

# Component Types

- Smart / Container
- Dumb / Presentational

# View Encapsulation

- Emulated :

  > Local and Global both CSS will apply
  > local CSS will override the global CSS rules

- ShadowDOM :

  > Only local CSS will apply to the component template
  > Global CSS will NOT affect the component template

- None :
  > Local CSS will affect the other part of the application

# View Encapsulation Scenerio

CompA : ViewEncapsulation.ShadowDOM - h1 Element
CompB : ViewEncapsulation.None - h1 Element - h1 {color : red}

# Pure Changes

let fruits = ["apple","banana"]; // xixox009

fruits = ["apple", "banana", "kiwi"]; // xix007xo

let obj = {name : "John"}

obj = { name : "Jenny" }

# Impure Changes

fruits.push("Oranges");

obj.name = "James"

---

# Form Types

- Template Driven : import FormsModule; HTML5 Validation
- Model Driven / Reactive Form : import ReactFormsModule; Angular Validation

# Form & Form Control State / Classes

- ngValid / ngInvalid
- ngPristine / ngDirty
- ngTouched / ngUntouched

- cnfPassword === password => true

---

# Services : key terminologies

- Consumer : the class to consume the dependency
- Dependency : class implments certain functionalities and is injectable
- Injector : DI Tree; injects the dependency in app
- Provider : register the service and creates injector scope
- Injector Token / DI Token : placeholder for service; uniquely identify the service

# 2 Principles of Service

- DIP -> Dependency Injection Principle
- SRP -> Single Responsibility Principle

# 2 Dependency Injection Tree

- Module DI Tree - Modules
- Element DI Tree - Components / Directives

# Injectable - providedIn Property

- root : singleton instance is available throughout the application
- platform : singleton instance is available to all MFE
- any : lazy loaded modules will have their own separate instance

# Resolution Modifiers in Services

- @Optional() - Null injector will provide NULL value instead of throwing Error
- @Self() - Injector will look the service registration in the self component
- @SkipSelf() - Injector will look the service registration in the anchestors of the hierarchy, NOT loog in the self component.
- @Host() - Injector will look for registration in the host component provider / viewProvider of the host component.

---

# Observables - keeps the eye on data source

- stream on which event occur at different time interval
- events carry data
- series of data
- are cancelable / unsubscribable
- support of powerful operators (50+ operators)
- lazily executed until subscribed
- are both async and sync (Scheduler - null, asapScheduler, asyncSchedule etc)
- hot or cold (default)
- Subject - both observer and observable
  > observer : next, error, complete
  > observable : subscribe, pipe
  > Subject Types : BehaviourSubject, ReplySubject, AsyncSubject
  > BehaviourSubject : Requires the seed value / initial Value
  > ReplaySubject : replay the number of previous emission
  > AsyncSubject : last emitted value once subject is completed
- Scheduler
- Operators : take, map, filter, ajax, mergeAll

Promises are one shot
Promises are Not cancelable
Promises are eagerly executed
Promises are async only
Promises does not supportted by operators

# configure JSON SERVER

- npm install json-server@0.17.4 -g
- json-server --version
- Create JSON File
- json-server --watch db.json

# Interceptors : intercept the request as well as response

# Routing Terminologies

- Route / Routes : configure the components with path
- RouterModule : enable creation of SPA; inform angular about route configuration
- RouterLink : does not submit page to server/ does not reload the page

http://localhost:4200/expenses/e001 -> EditExpenseComp
