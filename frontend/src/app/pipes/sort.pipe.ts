import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {
  transform(
    todoCollection: Array<{ label: string; status: string }>,
    sortOrder: string
  ): Array<{ label: string; status: string }> {
    if (sortOrder === '') {
      return todoCollection;
    }
    if (sortOrder === 'ascending') {
      return todoCollection.sort((a, b) => {
        if (a.label > b.label) {
          return +1;
        } else if (b.label > a.label) {
          return -1;
        } else {
          return 0;
        }
      });
    } else {
      return todoCollection.sort((a, b) => {
        if (a.label > b.label) {
          return -1;
        } else if (b.label > a.label) {
          return +1;
        } else {
          return 0;
        }
      });
    }
  }
}
