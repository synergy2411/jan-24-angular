import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
})
export class DialogComponent {
  @Input()
  btnTitle!: string;

  @Output() onOkEvent = new EventEmitter();

  onOK() {
    this.onOkEvent.emit();
  }
}
