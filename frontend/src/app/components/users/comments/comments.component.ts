import { Component, Input } from '@angular/core';
import { IComment } from 'src/app/model/coment.interface';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.css'],
})
export class CommentsComponent {
  @Input() comments!: Array<IComment>;

  tab = 1;

  addComment(comment: IComment) {
    this.comments.push(comment);
    this.tab = 1;
  }
}
